SE - PHP - Practical
================

We would like to invite you to do the Technical Assessment in which we will check coding standards, framework knowledge, how you approach the problem and your skills. You have **1 day** to complete the assessment.

This task requires the Developer to build a foundation application that do the following :
 
 

GUI Functionalities :
---------------------

1. Add grades in a school
2. Add classes in the school
3. List grades and classes in hierarchical structure
4. Add new students to the class
5. List students

REST API :
----------

1. Add new students to the class
2. List grades and classes
3. List students belonging to a specific class
4. Remove a class from the application

Hints :
-------

1. GUI/API routing
2. Documentation/ Comments
3. PSR compliance
4. Unit Tests
5. API Authentication (Optional)

You are free to use any PHP framework (Laravel preferred), 3rd party packages of your choice. The application **doesn't** have to have a pretty interface.

Once done, **add a instruction.md** with setup instructions and architectural decisions made.

If you use composer, **do not commit** the vendors folder.

Once done, submit a pull request and drop an email to <hr@pyxle.net>

